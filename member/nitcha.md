---
name: Nitcha Tothong(fame)
image: https://freight.cargo.site/w/1000/q/75/i/0c71b5a20ab2c9c70d40c9a1197aed8a0df848f65defca52a733e4e85e6a65f1/IMG_8707.jpg
links:
    website: https://nitcha.info/
    instagram: https://www.instagram.com/nitchafame/
---

Interdisciplinary artist, designer and researcher. Creative expression through art and technology.

