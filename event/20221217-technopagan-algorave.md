---
title: Technopagan Solstice Algorave
location: Wonderville, 1186 Broadway, Brooklyn, NY 11221
date: 2022-12-17 8:00 PM
duration: 4:30
image: /media/logo-white.svg
categories: [algorave, music, art, computers]
---

# Technopagan Solstice Rave
Dance in the dark. Create the spirits of tomorrow.

Lineup TBA
