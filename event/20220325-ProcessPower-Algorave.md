---
title: Process is Power Algorave
location: Wonderville
date: 2022-03-25 8:00 PM
link: https://withfriends.co/event/13767656/process_is_power_algorave
image: /image/2022-processispower-right.png
categories: [algorave, music, art, computers]
---

### Description
Synthesize, before your eyes, the brightest words of best delight.

Livecode.NYC presents a night of algo-rhythmic spectacular music and art! Listen, dance, absorb amazing visuals created on the spot by artists livecoding art and musicians livecoding music.

### Music & Vis
alsoknownasrox + char
casualsalad + gwenprime
Dan Gorelick + diane.af
mgs + emptyflash
LeHank + s4y
Reckoner+=Matthew + viz_wel
ColonelPanix + Edgardo
LuisaMei + Andrew Cotter
Azhad Syed + almdMilk
Naltroc + Edgardo
