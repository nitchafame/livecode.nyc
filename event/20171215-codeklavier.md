---
title: CodeKlavier
location: MAGNET at NYU Polytechnic School of Engineering
date: 2017-12-15 7:00 PM
image: /image/2018-codeklavier.png
link: https://www.facebook.com/events/1486524868320963
---

Programming with the piano as interface
[https://www.youtube.com/watch?v=ytpB8FB6VTU](https://www.youtube.com/watch?v=ytpB8FB6VTU)

The CodeKlavier is a system created by Felipe Ignacio Noriega and Anne Veinberg which allows the pianist to live code through playing the piano. “Hello World” is the first prototype piece created especially for live control of the actions of a mechanical toy piano (The Robot Toy Piano) through playing the interface of an acoustic midi piano.

The system is built on Javascript and SuperCollider. It can be downloaded from [https://narcode.github.io/codeklavier/](https://narcode.github.io/codeklavier/). All software is open source and publicly available.
